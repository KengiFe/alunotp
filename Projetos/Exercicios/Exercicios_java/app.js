const express = require('express');
const app = express();
const PORT = 8000;

app.get('/usuario', (req, res) => {
    res.send("MÉTODO GET - /USUARIO");
    console.log("MÉTODO GET - /USUARIO");
});

app.listen(PORT, () => {
    console.log(`PROJETO INICIADO NA PORTA ${PORT}`);
});